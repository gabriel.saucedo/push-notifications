import { Component } from '@angular/core';
import { SwPush } from '@angular/service-worker';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  respuesta: any;
  readonly VAPID_PUBLIC_KEY = 'BOhnPV1w2y6aeJDUfHcM1uxe-Va-psL-pfXOmMTBl71rWgj8vtKgG72SHBgQI93-K4tX7eD1VbiOfsLQtkV5ZYI';

  constructor(private swPush: SwPush) { }

  subscribeToNotifications() {
    this.swPush.requestSubscription(
      {
        serverPublicKey: this.VAPID_PUBLIC_KEY
      }
    ).then(response => {
      this.respuesta = response;
    })
      .catch(error => {
        this.respuesta = error;
      });
  }
}
