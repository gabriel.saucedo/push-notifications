const webpush = require('web-push');
const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser');

const app = express();

const vapidKeys = {
  "publicKey": "BOhnPV1w2y6aeJDUfHcM1uxe-Va-psL-pfXOmMTBl71rWgj8vtKgG72SHBgQI93-K4tX7eD1VbiOfsLQtkV5ZYI",
  "privateKey": "7f-TimLS4Vds8KwxF09VXaITxFgi5PMRG0mZi1zMmEM"
}

webpush.setVapidDetails(
  'mailto:example@yourdomain.org',
  vapidKeys.publicKey,
  vapidKeys.privateKey
);

const enviarNotificacion = (req, res) => {

  const pushSubscription = {
    endpoint: 'https://fcm.googleapis.com/fcm/send/eDdiijkKhgQ:APA91bF1OEcRiQAxalAzBKdFBOci0JvfuYg3CNNvGlVUlIRhcxKe82LaOhDrK9hmT16aeoKdnNA_2YAqkGaUBURYtNTHY8BMZUpT8Eg2Mpss6sv67qJINxHTYE4rJdsJRwYwpaQC3v8H',
    keys: {
      auth: 'mfhVyUovbx8UxPqyRvh7Rg',
      p256dh: 'BBUegQWOHruOW1L9ZqbndYJDpuNBuCWjBqxCWi-shlyu0A6DnumNZ2jUqZ-ybeKlJRt8quwIP20r5_v4whU45WI'
    }
  };

  const payload = {
    "notification": {
      "title": " SI funciona :'v",
      "body": "Demo notificaciones push",
      "vibrate": [100, 50, 100],
      "actions": [{
        "action": "explore",
        "title": "Go to the site"
      }]
    }
  }

  webpush.sendNotification(
    pushSubscription,
    JSON.stringify(payload))
    .then(res => {
      console.log('Enviado !!');
    }).catch(err => {
      console.log('Error', err);
    })

  res.send({ data: 'Se envio!!' })

}

app.route('/api/enviar').post(enviarNotificacion);


const httpServer = app.listen(9000, () => {
  console.log("HTTP Server running at http://localhost:" + httpServer.address().port);
});